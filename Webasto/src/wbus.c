/*
 * wbus.c
 *
 *  Created on: 01.03.2019
 *      Author: googy
 */
#include <avr/io.h>
#include <util/delay.h>
#include "wbus.h"
#include "uart.h"

extern volatile uint8_t time1;
extern uint8_t message[129];

struct heater_state hs;


// Pr�fsumme f�r die Nachricht
unsigned char checksum(unsigned char *buf, unsigned char len) {
    unsigned char chk = 0;
    for ( ; len != 0; len--) {
        chk ^= *buf++;
    }
    return chk;
}

enum exitcode request_data() {
    // request
    uart_puts("\xF4\x07\x50\x30\x0C\x0E\x11\x28\xA8");
    // 0x0C           15        Temperatur [15] - 50
    // 0x0E        17 18        Spannung Millivolt
    // 0x11        22 23        Heizleistung in Watt
    // 0x28           39        Betriebszustand, Liste siehe oben


    while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
    _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
    uart_flush();       // flush RX buffer, da tx und rx auf der gleichen Leitung, d.h. wir empfangen alles was wir senden

    // wait response
    if (rcv() == SUCCESS) {
        //decode
        hs.temperature = message[5] - 50;
        hs.voltage = message[7] << 8 | message[8];
        hs.power = message[10] << 8 | message[11];
        hs.running_state = message[13];
//      all into heater_state struct
        return SUCCESS;
    } else
        return FAIL;
}

enum exitcode rcv() {
    uint8_t state = 0;
    uint8_t i = 2;
    uint8_t length = 0;

    // max. 50ms auf Antwort warten, normalerweise antwortet der Zuheizer 15ms nach Ende der Anfrage
    // wenn nicht, versuche nochmal

    time1 = 0;   // jetzt stoppen wir die Zeit.
    while (1) {
        switch (state) {
        case 0: // warte auf erstes Byte 4F
            if (UCSR0A & (1 << RXC0)) { // Byte steht bereit, Zuheizer antwortet
                uint8_t c = UDR0;   // lese das empfangene Byte aus
                if (c == 0x4F) {    // Wenn es eine Antwort des Zuheizers ist
                    message[0] = 0x4F;  // f�r die checksum, ansonsten kann man auch drauf verzichten
                    state = 1;  // alles gut, als n�chstes empfangen wir die L�nge der Nachricht
                    time1 = 0;   // starte neuen Timeout
                    break;
                } else {
                    return FAIL;    // es wird etwas falsches empfangen, das erste empfangene Byte ist nicht richtig (0x4F)
                }
            }
            if (time1 > 3) {     // == nicht verwendbar, weil wir Inkremente auch verpassen k�nnen
                // Zeit abgelaufen ohne dass ein Byte empfangen wurde, Zuheizer antwortet nicht
                return NO_RESPONSE;
            }
            break;
        case 1: // empfange die L�nge der Nachricht
            if (UCSR0A & (1 << RXC0)) {
                length = UDR0;
                message[1] = length;
                state = 2;
                time1 = 0;   // starte neuen Timeout
                break;
            }
            if (time1 > 1) {     // == nicht verwendbar, weil wir Inkremente auch verpassen k�nnen
                // Zeit abgelaufen ohne dass ein Byte empfangen wurde
                return TIMEOUT;
            }
            break;
        case 2: // empfange Daten
            if (UCSR0A & (1 << RXC0)) {
                message [i] = UDR0;
                time1 = 0;   // starte neuen Timeout
                if (i == length + 1) {    // Empfang abgeschlossen
                    state = 3;
                    break;
                }
                i++;
                break;
            }
            if (time1 > 1) {     // == nicht verwendbar, weil wir Inkremente auch verpassen k�nnen
                // Zeit abgelaufen ohne dass ein Byte empfangen wurde
                return TIMEOUT;
            }
            break;
        case 3: // pr�fe Integrit�t
            if (checksum(message, message[1] + 1) == message[length + 1]) {
                return SUCCESS;
            }
            return FAIL;
        }
    }
}

uint8_t init_webasto() {
    /* wenn das USART eingeschaltet ist, bspw. TXEN0 ist gesetzt, dann wird die IO Funktion der
     * TX und RX Pins deaktiviert. Wir m�ssen jedoch den TX Pin f�r ca. 25-50 ms low und dann f�r die
     * gleiche Zeit wieder high setzen. Damit wird dem Zuheizer signalisiert, dass jetzt eine
     * �bertragung stattfindet. Diese Sequenz muss immer ein mal vor dem Senden nach einer l�ngerer
     * Pause durchgef�hrt werden. Wie lang diese timeout ist, ist bisher nicht bekannt.
     */
    UCSR0B = 0;     // Schalte UART aus, damit die Pins direkt angesteurt werden k�nnen
    // 25ms low, 25ms high
    DDRD |= (1 << PD1);     // beim ATMega328P ist TX vom USART an PD1
    PORTD &= ~(1 << PD1);
    _delay_ms(25);
    PORTD |= (1 << PD1);
    _delay_ms(25);

    uart_init();        // initialisiere USART mit 2400Baud 8E1


    /* Diese Nachricht fragt die Bussversion ab und ist FIXME nicht n�tig. Wird jedoch dazu verwendet
     * um zu erkennen ob der Zuheizer antwortet.
     * F4: Steuerung sendet
     * 03: Nachricht 3 Bytes lang
     * 51: lese Betriebsdaten
     * 0A: get Bus Version
     */
    uart_putb("\xF4\x03\x51\x0A\xAC", 5);
    while (!(UCSR0A & (1 << UDRE0))) {}     // warte bis alles gesendet wurde
    /* W-Bus hat nur eine Leitung f�r b eide Richtungen
     * Da die beiden Leitungen RX und TX miteinander verbunden sind, empfangen wir auch alles was wir
     * selbst senden. Diese Daten brauchen wir nicht. H�chstens f�r Fehler�berwachung wie Kurzschl�sse.
     * Zum jetzigen Moment wird dies nicht ausgenutzt. Daher m�ssen wir nch dem Senden alle empfangenen
     * Daten weg werfen und den Buffer leeren.
     * Die Antwort erfolgt ca. 15 ms nach dem Senden, bis dahin m�ssen wir den Buffer abger�umt haben.
     */

    _delay_ms(5);       //FIXME remove: Das warten hier ist nicht n�tig, da wir auf den vollst�ndigen Versand zuvor gewartet haben. Daher k�nnen wir davon ausgehen, dass das letzte Byte auf dem Bus ist und damit auch im RX Puffer angekommen.
    uart_flush();       // flush RX buffer, den Puffer zum Empfang freir�umen. Muss fertig sein bevor wir die Antwort erhalten.

    return rcv();       //TODO Timeout zur Fehlerbehandlung einbauen
}

/*
Initialisierung:
Busleitung default per Pullup auf high
25ms low
25ms high

Allgemein
1. Byte:
  F4
  ||
  |Empf�nger 4-Heizung, F-Software
  Sender F-Software, 4-Heizung

2. Byte:
  xx L�nge der Nachricht -> es folgen xx bytes

3. Byte:
  xx Operation 0x50, 0x51

weiter Parameter bei Senden
oder Daten beim Empfangen

letztes Byte Pr�fsumme aus XOR aller Bytes au�er der Pr�fsumme selbst

--51-0A-----------------------------------------------------------------------------
f4 03 51 0A ac
       |  |
       |  Bus Version
       Read Info

4f 04 d1 0a 41 d1
       |  |  |
       |  |  [Major Version][Minor Version] 4.1
       |  Bus Version
       bei Antwort erstes Bit setzen: 51 | 0b1000 0000
--------------------------------------------------------------------------------

--51-0C--------------------------------------------------------------------------------------------
f4 3 51 c aa
4f a d1 c 39 7c e4 2 4 0 0 3f
39: 0011 1001
    |||| |  |
    |||| |  ??ZH
    |||| on/off, nicht wirklich
    |||Standheizung
    ||Zuheizer
    |L�ftung
    Boost Modus

7C: 0111 1100
    |||| |||
    |||| ||External circulation pump control
    |||| |Combustion air fan (CAV)
    |||| Glow Plug (flame detector)
    |||Fuel pump (FP)
    ||Circulation pump (CP)
    |Vehicle fan relay (VFR)
    Yellow LED

E4: 1110 0100
    |||| ||||
    |||| |||Green LED present
    |||| ||Spark transmitter. Implies no Glow plug and thus no resistive flame detector.
    |||| |Solenoid valve present (coolant circuit switching)
    |||| Auxiliary drive indicator (whatever that means)
    |||Generator signal D+ present
    ||Combustion air fan level is in RPM instead of percent
    |(ZH)
    (ZH)

02: 0000 0010
         | |
         | CO2 calibration
         Operation indicator (OI)

04: 0000 0100
    |||| F(ZH)
    |||Heating energy is in watts (or if not set in percent and the value field must be divided by 2 to get the percent value)
    ||(ZH)
    |Flame indicator (FI)
    Nozzle Stock heating

00: 0000 0000
    |||
    ||Ignition (T15) flag present
    |Temperature thresholds available, command 0x50 index 0x11 (whatever that means)
    Fuel prewarming resistance and power can be read.

00: 0000 0000
           |
           Set value flame detector resistance (FW-SET), set value combustion air fan revolutions (BG-SET), set value output temperature (AT-SET)
----------------------------------------------------------------------------------

--56-------------------------------------------------------------------------------------------------------------------
Fehlerabfrage
f4 3 56 1 a0
      | |
      | lese Fehlerliste
      Fehlerbehandlung

Antwort
4f a d6 1 2 92 1 6 84 1 1 81
         |  | | | ------
         |  | | |   |
         |  | | |   zweiter fehler
         |  | | Z�hler, 6 mal aufgetreten
         |  | |
         |  | Status, 1 -> gespeichert, nicht aktuell
         |  ID 0x92 -> Kommando aufrechterhalten fehlgeschlagen
         Zwei Fehler vorhanden

bspw. 3 Fehler 4f d d6 1 3 12 1 4 92 1 6 84 1 1 90
keine          4f 4 d6 1 0 9c

l�sche Fehler
f4 3 56 3 a2
        |
        L�sche Liste
Antwort: OK
4f 3 d6 3 99
Antwort: l�schen nicht m�glich, "Vorbedingung nicht erf�llt", Heizung l�uft
4f 4 7f 56 22 40
---------------------------------------------------------------------------------------------------------------

--45-31------------------------------------------------------------------------------------------------------------
// Komponent test
f4  3 45 31 83
4f 13 c5 31  1  2  1  1  2  5  3  3  3  7  4  1  9  a  5  a a1
  Data format:
    1 byte component index
      1: Combustion Fan
      2: Fuel Pump
      3: Glow Plug
      4: Circulation Pump
      5: Vehicle Fan Relays
      6-8: Not used by TT-V
      9: Solenoid Valve
      10-14: Not used by TT-V
      15: Fuel Prewarming
      16: Not used by TT-V

    1 byte: Time to turn on device in seconds for testing.
    2 bytes: Magnitude value, according to selected component and WBUS code.
      Percent: 2nd byte ist 0.5 Prozent pro bit (2 ~> 1%).
      Hertz: 2nd byte 1/20 Hertz pro bit (20 ~> 1 Hertz).
      RPM: 1st byte MSB, 2nd byte LSB of RPM value.
------------------------------------------------------------------------------------------------------------------

--57----------------------------------------------------------------------------------------------------------------
CO2 calibration
f4 3 57  1 a1
4f 6 d7  1 b9  1 ff d8
         |  |  |  |
         |  |  |  maximum
         |  |  minimum
         |  current
         1: read CO2 calibration value

f4 3 57  3 vv xx
         |  |
         |  value (ein Byte)
         3: write CO2 calibration value
------------------------------------------------------------------------------------------------------------------

--38----------------------------------------------------------------------------------------------------------------
// Diagnose
f4 2 38 ce
4f b b8 d 0 0 4 b2 0 0 6 b8 f9  xor:f9
Command 0x38
  The Diagnosis issues this command once. It seems to be acknowlegded. No idea what it is for.
TX f4 02 38 ce
RX 4f 09 b8 0b 00 00 00 00 03 dd 2b
------------------------------------------------------------------------------------------------------------------

--53----------------------------------------------------------------------------------------------------------------
f4 3 53 2 a6
4f 4 7f 53 11 76  ERROR, not supported
Read other kind of data. Seemingly rather static data. Diagnosis does not do this so often.
------------------------------------------------------------------------------------------------------------------

--51-31-------------------------------------------------------------------------------------------------------------------
f4  3 51 31 97      Vorhandene Werte, die abgefragt werden k�nnen
4f 12 d1 31 1 f 17 6 3 4 5 7 8 b c d e 10 12 a9
--51-30-----------------------------------------------------------------------------------------------------------------
f4 12 51 30  1                                                                 f                                                                17                                                                 6                                                                 3           4     5              7           8           b                 c        d     e                         10    12       93      nutze vorherige Antwort zur abfrage
 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35
4f 86 d1 30  1 39 30 33 32 32 38 36 42 30 32  0  0  0  0  0  0  0  0  0  0  0  f 39 30 33 32 32 38 37 42 30 32  0  0  0  0  0  0  0  0  0  0  0 17 39 30 32 37 38 33 36 44 30 34  0  0  0  0  0  0  0  0  0  0  0  6 39 30 33 32 37 38 38 43 30 33  0  0  0  0  0  0  0  0  0  0  0  3  1 48 15  4  9  5  5  9  5  4  7 17 12  4  8 10  4 17  b 17 27 98 54 35  c 47 38  d 41  e 54 54 45 56 4f 20 32 47 10  d 12 88 12
                9  0  3  2  2  8  6  B  0  2                                      9  0  3  2  2  8  7  B  0  2                                      9  0  2  7  8  3  6  D  0  4                                      9  0  3  2  7  8  8  C  0  3                                      83989 328 21                    94500                                                     T  T  E  V  O __  2  G ??
                SG-Identnummer als ASCII                                          SW-Identnummer als ASCII
--------------------------------------------------------------------------------------------------------------------------

--50-30--------------------------------------------------------------------------------------------------------------------
    // Fehler
      0x3A    Kurzschluss???
      0x92    kommando aufrechterhalten fehlgeschlagen
      0x12    W-Bus Kommunikation fehlerhaft
      0x8c    Zu wenig Treibstoff???
      0x84    Unterspannung

    //Betriebszustand
      0x71    WAIT STATE
      0x73    MOTOR CHECK
      0x74    FLAME MONITOR COOLING
      0x76    PREGLOWING2
      0x77    PREGLOWING2 Partial Load Start
      0x79    START1
      0x7a    START2
      0x7b    START3
      0x7c    START4
      0x7d    START5
      0x7e    START6
      0x7f    GLOW PLUG RAMP
      0x80    FLAME MONITOR MEASUREMENT1
      0x81    FLAME MONITOR MEASUREMENT2
      0x82    START1 Partial Load Start
      0x83    START2 Partial Load Start
      0x84    START3 Partial Load Start
      0x85    START4 Partial Load Start
      0x86    START5 Partial Load Start
      0x87    START6 Partial Load Start
      0x88    GLOW PLUG RAMP Partial Load Start
      0x89    FLAME MONITOR MEASUREMENT1 Partial Load Start
      0x8a    FLAME MONITOR MEASUREMENT2 Partial Load Start
      0x90    BURN-OUT Full Load
      0x91    BURN-OUT Partial Load
      0x95    COOL1
      0x97    COOL2
      0x99    CONTINUOUS COOLANT TEMPERATURE CONTROL
      0x9a    CONTINUOUS COOLANT TEMPERATURE CONTROL - HOLD

    // sniffed
          0x61        75 76        Startz�hler ZH
          0x60        72 73        Startz�hler SH
          0x5F        69 70        Startz�hler
          0x5A     65 66 67        Betriebsdauer [66] Stunden, [67] Minuten
          0x59     61 62 63        Einschaltdauer ZH
          0x58     57 58 59        Einschaltdauer SH
          0x51     53 54 55        Dauer L�fter, bei mir immer 0

          0x28           39        Betriebszustand, Liste siehe oben

          0x1F           33        Frequenz Dosierpunpe [33] / 20
          0x1E        30 31        Brennluftgebl�se Umdrehungen/Minute [30] << 8 | [31]
          0x13        27 28        Flammenw�chter Milliohm
          0x12           25        Heizleistung 0-200%
          0x11        22 23        Heizleistung in Watt
          0x10           20        Flamme
          0x0E        17 18        Spannung Millivolt
          0x0C           15        Temperatur [15] - 50
          0x0A           13        1 Brennluftgebl�se, 2 Gl�hstift, 4 Dosierpumpe, 0x08 Circulation Pump (CP), 0x10 Vehicle Fan Relay (VFR), 0x20 Nozzle stock heating (NSH), 0x40 Flame indicator (FI)
          0x02            7        Zuheizen: 3 = an
          0x01            5        Standheizen: 3 = an



    Nachricht mit allen Elementen
    Abfrage:         F4 1F 50 30 01    02    03    04    0A    0C    0E       10    11       12    13       1E       1F    23    24    28    2A    2C    32    34       3D       51          58          59          5A          5F       60       61
    Byte-Nr:            //  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76
    // aus, kalt        // D0 30 01 00 02 00 03 00 04 00 0A 00                10 00 11 00 00 12 00 13 00 F8 1E 00 00 1F 00 23 00 24 00 28 71 2A 00 2C 00 32 00 34 04 56 3D 03 20 51 00 00 00 58 00 00 00 59 00 01 3A 5A 00 01 3A 5F 00 07 60 00 00 61 00 07
    // kurz vor flamme  // D0 30    03    00    00    00    05                   00    00 00    00                      6C    00    00    80    00    00    00    04 6A    03 20    00 00 00    00 05 0D    00 01 3A    00 07 0B    00 12    00 0B    00 07
    // voll heizen      // D0 30    03    00    00    00    05                   01    13 88    C8                      6C    00    00    99    00    00    00    04 6A    03 20    00 00 00    00 05 18    00 01 3A    00 07 16    00 13    00 0C    00 07
                        // D0 30 01 00 02 00 03 00 04 00 0A 00 0C 3C 0E 31 BA 10 00 11 00 00 12 00 13 01 02 1E 00 00 1F 00 23 00 24 00 28 71 2A 00 2C 00 32 00 34 04 6A
4f 4c d0 30 1 0 2 0 3 0 4 0 a 0 c 5f e 31 88 10 0 11 0 0 12 0 13 1 4c 1e 0 0 1f 0 23 0 24 0 28 71 2a 0 2c 0 32 0 34 4 6a 3d 3 20 51 0 0 0 58 0 10 2 59 0 6 1 5a 0 16 3 5f 0 44 60 0 29 61 0 1b 49  xor:49


 Nachricht von oben, jedoch alle bekannten Werte entfernt
                    //  2  3  8  9 10 11 34 35 36 37 40 41 42 43 44 45 46 47 48 49 50 51
// aus, kalt        // D0 30 03 00 04 00 23 00 24 00 2A 00 2C 00 32 00 34 04 56 3D 03 20
// kurz vor flamme  // D0 30    00    00    00    00    00    00    00    04 6A    03 20
// voll heizen      // D0 30    00    00    00    00    00    00    00    04 6A    03 20
//                              00    00    00    00    00    00    00    04 6A    03 20
//                                                            AA    76
//            uart_puts("\xF4\x04\x50\x30\x0E\x9E");  // read voltage only
//            uart_puts("\xF4\x04\x50\x30\x0C\x9C");  // read temp only
              uart_puts("\xF4\x06\x50\x30\x0C\x0E\x28\x??");  // read temp, voltage, state
*/












/*
Standheizen starten f�r 0x3B (59) Minuten
F4 03 21 3B ED
Antwort
4F 03 A1 3B D6

Erhaltung alle x Sekunden senden, sonst Abschaltung
F4 04 44 21 00 95
Antwort
4F 03 C4 01 89      01 -> ???
oder
4F 03 C4 FF 77      Zeit abgelaufen

Heizbetrieb abschalten
F4 02 10 E6
Antwort
4F 02 90 DD
*/

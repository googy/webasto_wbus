/*
 * wbus.h
 *
 *  Created on: 30.01.2019
 *      Author: googy
 */

#ifndef WBUS_H_
#define WBUS_H_

//
// W-Bus addresses_
// 0x0F : thermo Test Software
// 0x04 : heating device
// 0x03 : 1533 Timer
// 0x02 : Telestart
//
// address as client
#define WBUS_CLIENT_ADDR    0x0F
// address as host
#define WBUS_HOST_ADDR      0x04

/* W-Bus commands */
#define WBUS_CMD_OFF        0x10 /* no data */
#define WBUS_CMD_ON         0x20 /* For all CMD_ON_x : 1 byte = time in minutes */
#define WBUS_CMD_ON_PH      0x21 /* Parking Heating */
#define WBUS_CMD_ON_VENT    0x22 /* Ventilation */
#define WBUS_CMD_ON_SH      0x23 /* Supplemental Heating */
#define WBUS_CMD_CP         0x24 /* Circulation Pump external control */
#define WBUS_CMD_BOOST      0x25 /* Boost mode */
#define WBUS_CMD_SL_RD      0x32 /* Telestart system level read */
#define WBUS_CMD_SL_WR      0x33 /* Telestart system level write */
#define WBUS_CMD_EEPROM_RD  0x35 /* READ_EEPROM [address].
                                 Response length 2 byte. Reads two bytes from
                                 eeprom memory, from address given as parameter.
                                 The eeprom is accessible from address 0x00 to 0x1f.
                                 In eeprom dump I found customer ID Number,
                                 registered remote fob codes, telestart system level etc. */
#define WBUS_CMD_EEPROM_WR  0x36 /* WRITE_EEPROM [address],[byte1],[byte2]
                                 Write two bytes to eeprom. Address, byte1, byte2
                                 as parameter. */
#define WBUS_CMD_U1         0x38 /* No idea. */
#define WBUS_TS_REGR        0x40 /* discovered by pczepek, Response length 0 byte.
                                 Parameter 0-0x0f. After issuing command,
                                 press remote fob OFF button to register.
                                 You can register max 3 remote fob. */
#define WBUS_CMD_FP         0x42 /* Fuel prime. data 0x03 <2 bytes time in seconds / 2> */
#define WBUS_CMD_CHK        0x44 /* Check current command (0x20,0x21,0x22 or 0x23) */
#define WBUS_CMD_TEST       0x45 /* <1 byte sub system id> <1 byte time in seconds> <2 bytes value> */
#define WBUS_CMD_QUERY      0x50 /* Read operational information registers */
#define WBUS_CMD_IDENT      0x51 /* Read device identification information registers */
#define WBUS_CMD_OPINFO     0x53 /* 1 byte, operational info index. */
#define WBUS_CMD_ERR        0x56 /* Error related commands */
#define WBUS_CMD_CO2CAL     0x57 /* CO2 calibration */
#define WBUS_CMD_DATASET    0x58 /* (Not Webasto) data set related commands */

/* 0x50 Command parameters */
/* Status flags. Bitmasks below. STAxy_desc means status "x", byte offset "y", flag called 2desc" */
#define QUERY_STATUS0       0x02
#define     STA00_SHR       0x10 /*!< Supplemental heater request*/
#define     STA00_MS        0x01 /*!< Main switch */
#define     STA01_S         0x01 /*!< Summer season */
#define     STA02_D         0x10 /*!< Generator signal D+ */
#define     STA03_BOOST     0x10 /*!< boost mode */
#define     STA03_AD        0x01 /*!< auxiliary drive */
#define     STA04_T15       0x01 /*!< ignition (terminal 15) */

#define QUERY_STATUS1       0x03
#define     STA10_CF        0x01 /*!< Combustion Fan    */
#define     STA10_GP        0x02 /*!< Glühkerze     */
#define     STA10_FP        0x04 /*!< Fuel Pump             */
#define     STA10_CP        0x08 /*!< Circulation Pump  */
#define     STA10_VF        0x10 /*!< Vehicle Fan Relay */
#define     STA10_NSH       0x20 /*!< Nozzle stock heating  */
#define     STA10_FI        0x40 /*!< Flame indicator       */

#define QUERY_OPINFO0       0x04 /* Fuel type, max heat time and factor for shortening ventilation time (but details are unclear) */
#define     OP0_FUEL        0      /*!< 0x0b: gasoline, 0x0d: diesel, 0:neutral */
#define     OP0_TIME        1      /*!< max heating time / 10 in minutes */
#define     OP0_FACT        2      /*!< ventilation shortening factor (?) */

#define QUERY_SENSORS       0x05 /*!< Assorted sensors. 8 bytes. Byte offsets below. */
#define     SEN_TEMP        0   /*!< Temperature with 50�C offset (20�C is represented by 70) */
#define     SEN_VOLT        1   /*!< 2 bytes Spannung in mili Volt, big endian */
#define     SEN_FD          3   /*!< 1 byte Flame detector flag */
#define     SEN_HE          4   /*!< 2 byte, heating power, in percent or watts (semantic seems wrong, heating energy?) */
#define     SEN_GPR         6   /*!< 2 byte, glow plug resistance in mili Ohm. */

#define QUERY_COUNTERS1     0x06
#define     WORK_HOURS      0 /*!< Working hours     */
#define     WORK_MIN        2 /*!< Working minutes   */
#define     OP_HOURS        3 /*!< Operating hours   */
#define     OP_MIN          5 /*!< Operating minutes */
#define     CNT_START       6 /*!< Start counter     */

#define QUERY_STATE         0x07
#define     OP_STATE        0
#define     OP_STATE_N      1
#define     DEV_STATE       2
/* 3 more unknown bytes */
#define WB_STATE_BO         0x00 /* Burn out */
#define WB_STATE_DEACT1     0x01 /* Deactivation */
#define WB_STATE_BOADR      0x02 /* Burn out ADR (has something to due with hazardous substances transportation) */
#define WB_STATE_BORAMP     0x03 /* Burn out Ramp */
#define WB_STATE_OFF        0x04 /* Off state */
#define WB_STATE_CPL        0x05 /* Combustion process part load */
#define WB_STATE_CFL        0x06 /* Combustion process full load */
#define WB_STATE_FS         0x07 /* Fuel supply */
#define WB_STATE_CAFS       0x08 /* Combustion air fan start */
#define WB_STATE_FSI        0x09 /* Fuel supply interruption */
#define WB_STATE_DIAG       0x0a /* Diagnostic state */
#define WB_STATE_FPI        0x0b /* Fuel pump interruption */
#define WB_STATE_EMF        0x0c /* EMF measurement */
#define WB_STATE_DEB        0x0d /* Debounce */
#define WB_STATE_DEACTE     0x0e /* Deactivation */
#define WB_STATE_FDI        0x0f /* Flame detector interrogation */
#define WB_STATE_FDC        0x10 /* Flame detector cooling */
#define WB_STATE_FDM        0x11 /* Flame detector measuring phase */
#define WB_STATE_FDMZ       0x12 /* Flame detector measuring phase ZUE */
#define WB_STATE_FAN        0x13 /* Fan start up */
#define WB_STATE_GPRAMP     0x14 /* Glow plug ramp */
#define WB_STATE_LOCK       0x15 /* Heater interlock */
#define WB_STATE_INIT       0x16 /* Initialization   */
#define WB_STATE_BUBLE      0x17 /* Fuel bubble compensation */
#define WB_STATE_FANC       0x18 /* Fan cold start-up */
#define WB_STATE_COLDR      0x19 /* Cold start enrichment */
#define WB_STATE_COOL       0x1a /* Cooling */
#define WB_STATE_LCHGUP     0x1b /* Load change PL-FL */
#define WB_STATE_VENT       0x1c /* Ventilation */
#define WB_STATE_LCHGDN     0x1d /* Load change FL-PL */
#define WB_STATE_NINIT      0x1e /* New initialization */
#define WB_STATE_CTRL       0x1f /* Controlled operation */
#define WB_STATE_CIDDLE     0x20 /* Control iddle period */
#define WB_STATE_SSTART     0x21 /* Soft start */
#define WB_STATE_STIME      0x22 /* Savety time */
#define WB_STATE_PURGE      0x23 /* Purge */
#define WB_STATE_START      0x24 /* Start */
#define WB_STATE_STAB       0x25 /* Stabilization */
#define WB_STATE_SRAMP      0x26 /* Start ramp    */
#define WB_STATE_OOP        0x27 /* Out of power  */
#define WB_STATE_LOCK2      0x28 /* Interlock     */
#define WB_STATE_LOCKADR    0x29 /* Interlock ADR (Australian design rules) */
#define WB_STATE_STABT      0x2a /* Stabilization time */
#define WB_STATE_CHGCTRL    0x2b /* Change to controlled operation */
#define WB_STATE_DECIS      0x2c /* Decision state */
#define WB_STATE_PSFS       0x2d /* Prestart fuel supply */
#define WB_STATE_GLOW       0x2e /* Glowing */
#define WB_STATE_GLOWP      0x2f /* Glowing power control */
#define WB_STATE_DELAY      0x30 /* Delay lowering */
#define WB_STATE_SLUG       0x31 /* Sluggish fan start */
#define WB_STATE_AGLOW      0x32 /* Additional glowing */
#define WB_STATE_IGNI       0x33 /* Ignition interruption */
#define WB_STATE_IGN        0x34 /* Ignition */
#define WB_STATE_IGNII      0x35 /* Intermittent glowing */
#define WB_STATE_APMON      0x36 /* Application monitoring */
#define WB_STATE_LOCKS      0x37 /* Interlock save to memory */
#define WB_STATE_LOCKD      0x38 /* Heater interlock deactivation */
#define WB_STATE_OUTCTL     0x39 /* Output control */
#define WB_STATE_CPCTL      0x3a /* Circulating pump control */
#define WB_STATE_INITUC     0x3b /* Initialization uP */
#define WB_STATE_SLINT      0x3c /* Stray light interrogation */
#define WB_STATE_PRES       0x3d /* Prestart */
#define WB_STATE_PREIGN     0x3e /* Pre-ignition */
#define WB_STATE_FIGN       0x3f /* Flame ignition */
#define WB_STATE_FSTAB      0x40 /* Flame stabilization */
#define WB_STATE_PH         0x41 /* Combustion process parking heating */
#define WB_STATE_SH         0x42 /* Combustion process suppl. heating  */
#define WB_STATE_PHFAIL     0x43 /* Combustion failure failure heating */
#define WB_STATE_SHFAIL     0x44 /* Combustion failure suppl. heating  */
#define WB_STATE_OFFR       0x45 /* Heater off after run */
#define WB_STATE_CID        0x46 /* Control iddle after run */
#define WB_STATE_ARFAIL     0x47 /* After-run due to failure */
#define WB_STATE_ARTCTL     0x48 /* Time-controlled after-run due to failure */
#define WB_STATE_LOCKCP     0x49 /* Interlock circulation pump */
#define WB_STATE_CIDPH      0x4a /* Control iddle after parking heating */
#define WB_STATE_CIDSH      0x4b /* Control iddle after suppl. heating  */
#define WB_STATE_CIDHCP     0x4c /* Control iddle period suppl. heating with circulation pump */
#define WB_STATE_CPNOH      0x4d /* Circulation pump without heating function */
#define WB_STATE_OV         0x4e /* Waiting loop overvoltage */
#define WB_STATE_MFAULT     0x4f /* Fault memory update */
#define WB_STATE_WLOOP      0x50 /* Waiting loop */
#define WB_STATE_CTEST      0x51 /* Component test */
#define WB_STATE_BOOST      0x52 /* Boost */
#define WB_STATE_COOL2      0x53 /* Cooling */
#define WB_STATE_LOCKP      0x54 /* Heater interlock permanent */
#define WB_STATE_FANIDL     0x55 /* Fan iddle */
#define WB_STATE_BA         0x56 /* Break away */
#define WB_STATE_TINT       0x57 /* Temperature interrogation */
#define WB_STATE_PREUV      0x58 /* Prestart undervoltage */
#define WB_STATE_AINT       0x59 /* Accident interrogation */
#define WB_STATE_ARSV       0x5a /* After-run solenoid valve */
#define WB_STATE_MFLTSV     0x5b /* Fault memory update solenoid valve */
#define WB_STATE_TCARSV     0x5c /* Timer-controlled after-run solenoid valve */
#define WB_STATE_SA         0x5d /* Startup attempt */
#define WB_STATE_PREEXT     0x5e /* Prestart extension */
#define WB_STATE_COMBP      0x5f /* Combustion process */
#define WB_STATE_TIARUV     0x60 /* Timer-controlled after-run due to undervoltage */
#define WB_STATE_MFLTSW     0x61 /* Fault memory update prior switch off */
#define WB_STATE_RAMPFL     0x62 /* Ramp full load */
    /*byte1 Operating state state number*/
    /*byte2 Device state*/
#define WB_DSTATE_STFL      0x01 /* STFL */
#define WB_DSTATE_UEHFL     0x02 /* UEHFL */
#define WB_DSTATE_SAFL      0x04 /* SAFL   */
#define WB_DSTATE_RZFL      0x08 /* RZFL */
    /*byte3,4,5: Unknown*/
#define QUERY_DURATIONS0    0x0a /* 24 bytes */

#define QUERY_DURATIONS1    0x0b /* 6 bytes*/
#define     DUR1_PH         0 /* Parking heating duration, hh:m */
#define     DUR1_SH         3 /* Supplemental heating duration hh:m */

#define QUERY_COUNTERS2     0x0c
#define     STA3_SCPH       0   /*!< 2 bytes, parking heater start counter  */
#define     STA3_SCSH       2   /*!< 2 bytes, supplemtal heater start counter */
#define     STA34_FD        0x00    /*!< Flame detected  */

#define QUERY_STATUS2       0x0F
#define     STA2_GP         0 /* glow plug (ignition/flame detection)*/
#define     STA2_FP         1 /* fuel pump */
#define     STA2_CAF        2 /* combustion air fan */
#define     STA2_U0         3 /* unknown */
#define     STA2_CP         4 /* (coolant) circulation pump */

#define QUERY_OPINFO1       0x11
#define     OP1_THI         0 /* Lower temperature threshold */
#define     OP1_TLO         1 /* Higher temperature threshold */
#define     OP1_U0          2

#define QUERY_DURATIONS2    0x12 /* 3 bytes */
#define     DUR2_VENT       0 /* Ventilation duration hh:m */

#define QUERY_FPW           0x13 /*!< Fuel prewarming. May not be available. See wbcode */
#define     FPW_R           0   /*!< 2 bytes: Current fuel prewarming PTC resistance in mili ohm, big endian */
#define     FPW_P           2   /*!< 2 bytes: Currently applied fuel prewarming power in watts, big endian */

/* 0x51 Command parameters */
#define IDENT_DEV_ID        0x01 /*!< Device ID Number */
#define IDENT_HWSW_VER      0x02 /*!< Hardware version (KW/Jahr), Software version, Software version EEPROM, 6 bytes */
#define IDENT_DATA_SET      0x03 /*!< Data Set ID Number */
#define IDENT_DOM_CU        0x04 /*!< Control Unit Herstellungsdatum (Tag monat jahr je ein byte) */
#define IDENT_DOM_HT        0x05 /*!< Heizer Herstellungsdatum (Tag monat jahr je ein byte) */
#define IDENT_TSCODE        0x06 /*!< Telestart code */
#define IDENT_CUSTID        0x07 /*!< Customer ID Number (Die VW Teilenummer als string und noch ein paar Nummern dran) + test sig */
#define IDENT_U0            0x08 /*!< ? */
#define IDENT_SERIAL        0x09 /*!< Serial Number */
#define IDENT_WB_VER        0x0a /*!< W-BUS version. Antwort ergibt ein byte. Jedes nibble dieses byte entspricht einer Zahl (Zahl1.Zahl2) */
#define IDENT_DEV_NAME      0x0b /*!< Device Name: Als character string zu interpretieren. */
#define IDENT_WB_CODE       0x0c /*!< W-BUS code. 7 bytes. This is sort of a capability bit field */

/* W-Bus code bits */
#define WB_CODE_0            0 /* Unknown supplemental heater feature */
#define WB_CODE_ON           3 /* on/off switch capability */
#define WB_CODE_PH           4 /* Parking heater capability */
#define WB_CODE_SH           5 /* Supplemental heater capability */
#define WB_CODE_VENT         6 /* Ventilation capability */
#define WB_CODE_BOOST        7 /* Boost capability */

#define WB_CODE_ECPC         9 /* External circulation pump control */
#define WB_CODE_CAV         10 /* Combustion air fan (CAV) */
#define WB_CODE_GP          11 /* Glow Plug (flame detector) */
#define WB_CODE_FP          12 /* Fuel pump (FP) */
#define WB_CODE_CP          13 /* Circulation pump (CP) */
#define WB_CODE_VFR         14 /* Vehicle fan relay (VFR) */
#define WB_CODE_LEDY        15 /* Yellow LED */

#define WB_CODE_LEDG        16 /* Green LED present */
#define WB_CODE_ST          17 /* Spark transmitter. Implies no Glow plug and thus no resistive flame detection */
#define WB_CODE_SV          18 /* Solenoid valve present (coolant circuit switching) */
#define WB_CODE_DI          19 /* Auxiliary drive indicator (whatever that means) */
#define WB_CODE_D           20 /* Generator signal D+ present */
#define WB_CODE_CAVR        21 /* Combustion air fan level is in RPM instead of percent */
#define WB_CODE_22          22 /* (ZH) */
#define WB_CODE_23          23 /* (ZH) */

#define WB_CODE_CO2         25 /* CO2 calibration */
#define WB_CODE_OI          27 /* Operation indicator (OI) */

#define WB_CODE_32          32 /* (ZH) */
#define WB_CODE_33          33 /* (ZH) */
#define WB_CODE_34          34 /* (ZH) */
#define WB_CODE_35          35 /* (ZH) */
#define WB_CODE_HEW         36 /* Heating energy is in watts (or if not set in percent and the value field must be divided by 2 to get the percent value) */

#define WB_CODE_37          37 /* (ZH) */
#define WB_CODE_FI          38 /* Flame indicator (FI) */
#define WB_CODE_NSH         39 /* Nozzle Stock heating (NSH) */

#define WB_CODE_T15         45 /* Ignition (T15) flag present */
#define WB_CODE_TTH         46 /* Temperature thresholds available, command 0x50 index 0x11 */
#define WB_CODE_VPWR        47 /* Fuel prewarming resistance and power can be read. */

#define WB_CODE_SET         57 /* 0x02 Set value flame detector resistance (FW-SET), set value combustion air fan revolutions (BG-SET), set value output temperature (AT-SET) */

#define IDENT_SW_ID         0x0D /*!< Software ID */

/* 053 operational info indexes */
#define OPINFO_LIMITS       02
/*
  data format:
 1 byte:  no idea
 2 bytes: Minimum voltage threshold in milivolts
 4 bytes: no idea
 1 byte:  minimum voltage detection delay (seconds)
 2 bytes: maximum voltage threshold in milivolts
 4 bytes: no idea
 1 byte:  maximum voltage detection delay (seconds
 */

/* 0x56 Error code operand 0 */
#define ERR_LIST            1 /* send not data. answer is n, code0, counter0-1, code1, counter1-1 ... coden, countern-1 */
#define ERR_READ            2 /* send code. answer code, flags, counter ... (err_info_t) */
#define ERR_DEL             3 /* send no data. answer also no data. */

#define CO2CAL_READ         1 /* 3 data bytes: current value, min value, max value. */
#define CO2CAL_WRITE        3 /* 1 data byte: new current value. */

/* Component test device definitions */
#define WBUS_TEST_CF         1 /*!< Combustion Fan */
#define WBUS_TEST_FP         2 /*!< Fuel Pump */
#define WBUS_TEST_GP         3 /*!< Glow Plug */
#define WBUS_TEST_CP         4 /*!< Circulation Pump */
#define WBUS_TEST_VF         5 /*!< Vehicle Fan Relays */
#define WBUS_TEST_SV         9 /*!< Solenoid Valve */
#define WBUS_TEST_NSH       13 /*!< Nozzel Stock Heating */
#define WBUS_TEXT_NAC       14 /*!< Nozzle air compressor (not standart, POELI specific) */
#define WBUS_TEST_FPW       15 /*!< Fuel Prewarming */

enum exitcode{
    FAIL =          0,
    SUCCESS =       1,
    NO_RESPONSE =   2,
    TIMEOUT =       3,
    CORRUPT =       4
};

struct heater_state {
    uint16_t voltage;
    uint8_t temperature;
    uint8_t running_state;
    uint16_t power;
    uint8_t operating_mode;
};

// Prüfsumme für die Nachricht
unsigned char checksum(unsigned char *buf, unsigned char len);
enum exitcode rcv();
uint8_t init_webasto();
enum exitcode request_data();

#endif /* WBUS_H_ */






/*
 * Fehler:
0x00    <kein Fehler>                                                          kein Fehler
0x01    interner CPU-Fehler                                                    Steuergerät defekt, Fehlerhafte EOL-Programmierung\noder Umwälztemperatursensor fehlerhaft
0x02    nach Startwiederholung keine Flamme                                    Auch nach Startwiederholung hat sich keine Flamme gebildet́
0x03    kein Start nach Flammabbruch                                          Die Flamme ist währ des Betriebs erloschen und hat sich auch nach einem\nStartversuch (ggf. einschl. Wiederholstart) nicht mehr ausgebildetC
0x04    Überspannungsabschaltung                                              Die Betriebsspannung war zu lange über dem zulässigen Maximalwert@
0x05    Flamme vor Brennbetrieb                                               Der Brennraumsensor hat vor dem Brennbetrieb eine Flamme erkanntF
0x06    Überhitzungsabschaltung'                                                   Die Überhitzungsverriegelung hat angesprochen (Heizgerät überhitzt)+
0x07    Heizgeräteverriegelung wurde aktiviert                                    Die Heizgeräteverriegelung wurde aktiviert;
0x08    DP Kurzschluss nach Masse                                             Der Dosierpumpenstromkreis hat einen Kurzschluss nach Masse^
0x09    BLG Kurzschluss                                                       Das Brennluftgebläse hat einen Kurzschluss nach Masse\noder der Gebläsemotor ist überlastetJ
0x0A    GS Kurzschluss                                                            Der Glühstift- / Flammwächterstromkreis hat einen Kurzschluss nach Massei
0x0B    UP Kurzschluss,                                                            Die geschaltete Leitung der Umwälzpumpe hat einen Kurzschluss nach Masse\noder der Motor ist überlastetҁ
0x0C    Kommunikation mit Klimagerät nicht möglich                            Die Kommunikation mit dem Klimasteuergerät ist nicht möglich\n(Fehler wird gesetzt, wenn Antwort vom Klimasteuergerät länger als 10 sec ausbleibt)\nBei Auftreten des Fehlers kein Heiz- oder Lüftungsbetrieb<
0x0D    grüne LED Kurzschluss                                                 Der Stromkreis 'Grüne LED' hat einen Kurzschluss nach Masse;
0x0E    gelbe LED Kurzschluss,                                                 Der Stromkreis 'Gelbe LED' hat einen Kurzschluss nach Masseс
0x0F    Typ- und Ausstattungscodesignal ausgeblieben%                          Bei wachem Bus ist das Typ- und Ausstattungscodesignal (Diesel/Benzin,\nFunkfernbedienungsempfänger vorhanden/nicht vorhanden) länger als 3 sec ausgeblieben.\nBei Auftreten des Fehlers unveränderter Betrieb>
0x10    Kühlmittelumschaltventil Kurzschluss(                                  Das Kühlmittelumschaltventil hat einen Kurzschluss nach Masse[
0x11    Datensatz fehlerhaft (falsche Kodierung)                              Falsch codiertes Steuergerät bzw. falsches Heizgerät (bezüglich Kraftstoffart) eingebautp
0x12    W-Bus Kommunikation fehlerhaft                                            W-Bus Kommunikation fehlerhaft (Busstörung, Protokollfehler).\nBei Auftreten des Fehlers unveränderter Betrieb?
0x13    FZG Kurzschluss                                                       Der Fahrzeuggebläsestromkreis het einen Kurzschluss nach Masse?
0x14    Temperatur-Sensor Kurzschluss"                                         Der Temperatursensorstromkreis het einen Kurzschluss nach Masse+
0x15    BM-Blockierschutz hat angesprochen"                                        Brennermotorblockierschutz hat angesprochen,
0x16    Batterie-Trennschalter KurzschlussÜ                                   Batterietrennschalter Kurzschluss nach MasseJ
0x17    HS-Gradient zu groß                                                   Falsche Applikation oder unzulässige Verdämmung (ÜHS-Gradient zu groß)
0x18    Kundenbus fehlerhaft                                                  Kundenbus fehlerhaftK
0x19    GS Kurzschluss                                                            Der Glühstift- / Zündfunkgeberstromkreis hat einen Kurzschluss nach Masse>
0x1A    FW Kurzschluss                                                            Der Brennraumsensorstromkreis hat einen Kurzschluss nach MasseE
0x1B    Überhitzungssensor Kurzschluss                                            Die geschaltete Leitung des Elements hat einen Kurzschluss nach MasseH
0x1C    EKP Kurzschluss-                                                           Die elektronische Kraftstoffpunpe (EKP) hat einen Kurzschluss nach MasseQ
0x1D    Kraftstoffabsperrventil Shed-Test Kurzschluss                         Das Kraftstoffabsperrventil Shed-Test Stromkreis hat einen Kurzschluss nach Masse5
        Brennstoffsensor Kurzschluss                                          Der Brennstoffsensor hat einen Kurzschluss nach Masse;
        Düsenstockheizung Kurzschluss                                         Die Düsenstockvorwärmung hat einen Kurzschluss nach Masse4
        Betriebsanzeige Kurzschluss                                           Die Betriebsanzeige hat einen Kurzschluss nach Masse1
        Flammanzeige Kurzschluss,                                              Die Flammanzeige hat einen Kurzschluss nach Masse6
        Referenzwiderstand beim Start nicht erreicht                          Der Referenzwiderstand wurde beim Start nicht erreicht%
        Crashverriegelung aktiviert                                           Die Crashverriegelung wurde aktiviert6
        Fahrzeug im KS-Reservebetrieb                                         Das Fahrzeug befindet sich im Kraftstoffreservebetrieb:
        BS-Vorwärmung Kurzschluss*                                             Die Brennstoffvorwärmung hat einen Kurzschluss nach MasseB
        Leiterplattentemperatur-Sensor Kurzschluss!                                Der Leiterplattentemperatursensor hat einen Kurzschluss nach Masse3
        Masseverbindung zum SG fehlerhaft.                                     Die Masseverbindung zum Steuergerät ist fehlerhaft7
        Bordnetzmanager meldet Bordspannung zu niedrig)                            Bordnetz Energiemanager meldet zu niedrige Bordspannung-
        Leitungsbefüllung ist noch nicht erfolgt                              Die Leitungsbefüllung ist noch nicht erfolgtV
        Fehler im Funktelegramm                                               Fehler im Funktelegramm (Telestart: CRC-Fehler, Fehlerkorrektur war nicht erfolgreich)
        Telestart nicht angelernt                                             Telestart nicht angelernt*
        Drucksensor Kurzschluss                                               Der Drucksensor hat Kurzschluss nach Masse6
        BLG-Stromkreis fehlerhaft                                             Gebläsemotordrehzahl liegt unter dem zu erwarten WertA
        GF-Stromkreis fehlerhaft                                              Glühstiftwiderstand liegt außerhalb des gültigen Wertebereichs]
        Flammabbruch => Neustart*                                              Die Flamme ist währ des Betriebs erloschen. \nEs wird ein weiterer Startversuch ausgeführt.&
        FZG Unterbrechung oder Kurzschluss nach UB<                                Keine weiteren Informationen vorhanden&
        Abgastemperatursensor Unterbrechung oder Kurzschluss nach UB          Keine weiteren Informationen vorhanden9
        kein Start aus Regelpause$                                             Der Temperaturfühler liefert einen fehlerhaften Messwert>
        Glühstift/Flammwächter unplausibel                                    Der Glühstift / Flammwächter liefert ein unplausibles Signal
        Ersatzwerte eingesetzt                                                     Ersatzwerte wurden eingesetzt;
        EOL-Programmierung nicht erfolgt                                      Die EOL-Programmierung des Steuergerätes ist nicht erfolgt-
        Temperatursicherung Kurzschluss.                                           Die Temperatursicherung hat einen Kurzschluss&
        Wassertemperatur beim IOS zu hoch (kein Start)$                            Keine weiteren Informationen vorhanden&
        Erststart fehlgeschlagen => Neustart1                                  Keine weiteren Informationen vorhanden&
        Erststart fehlgeschlagen => kein Neustart erlaubt                     Keine weiteren Informationen vorhanden&
        W-Bus/LIN-Bus Kurzschluss                                             Keine weiteren Informationen vorhanden&
        Abgastemperatur zu niedrig                                            Keine weiteren Informationen vorhanden&
        Ladungspumpe überlastet"                                                   Keine weiteren Informationen vorhanden&
        MCU-Spannungsversorgung fehlerhaft                                        Keine weiteren Informationen vorhanden&
        SBC überhitzt                                                          Keine weiteren Informationen vorhanden&
        falsche Datensatzversion geladen6                                      Keine weiteren Informationen vorhandenJ
        Glühstift/Zündfunkengeber - Glühwendel unterbrochen                   Einer der Heizkreise des Glühstiftes / Zündfunkengebers ist unterbrochen
        Oszillatorstörung&                                                     Oszillatorstörung&
        Signalleitung Netzspannung Kurzschluss;                                    Keine weiteren Informationen vorhanden&
        Ausgang Überhitzungsrelais Überhitzung oder Unterbrechung'             Keine weiteren Informationen vorhanden&
        Ausgang Überhitzungsrelais Kurzschluss0                                    Keine weiteren Informationen vorhanden&
        Relaisbox fehlerhaft (Kurzschluss/Unterbrechung)                      Keine weiteren Informationen vorhanden&
        Steuerausgang A Kurzschluss                                           Keine weiteren Informationen vorhanden&
        Steuerausgang A Unterbrechung!                                         Keine weiteren Informationen vorhanden&
        Außentemperatursensor fehlerhaft                                      Keine weiteren Informationen vorhanden&
        Steuerausgang B Kurzschluss                                           Keine weiteren Informationen vorhanden&
        Steuerausgang B Unterbrechung                                         Keine weiteren Informationen vorhanden&
        230 V Heizelement                                                     Keine weiteren Informationen vorhandenA
        Überspannung Komponentenschutz!                                            Abschaltung bei extrem hoher Überspannung zum Komponentenschutz..
        Abgastemperatursensor Kurzschluss(                                     Abgas Temperatur Sensor Kurzschluss nach Masse&
        Timer DP_max überschritten (kein Start)                               Keine weiteren Informationen vorhanden&
        Abgastemperatur zu hoch3                                                   Keine weiteren Informationen vorhanden&
        User-Interface im Idle-Mode (kein Telegrammverkehr)"                       Keine weiteren Informationen vorhanden&
        User-Interface mit Telegrammfehler2                                        Keine weiteren Informationen vorhanden&
        User-Interface sendet nicht definierte Betriebsart&                        Keine weiteren Informationen vorhanden&
        Heizluftgebläse Statusmeldung negativ2                                 Keine weiteren Informationen vorhanden&
        Heizluftgebläse Statusleitung Kurzschluss nach UB$                     Keine weiteren Informationen vorhanden&
        Wassertemperatursensor Unterbrechung*                                  Keine weiteren Informationen vorhanden&
        Wassertemperatursensor Kurzschluss nach UB#                                Keine weiteren Informationen vorhanden&
        Überhitzung Wassertemperatursensor0                                        Keine weiteren Informationen vorhanden&
        Gradientenüberschreitung Wassertemperatursensor$                           Keine weiteren Informationen vorhanden&
        Überhitzung Ausblastemperatursensor1                                       Keine weiteren Informationen vorhanden&
        Gradientenüberschreitung Ausblastemperatursensor*                      Keine weiteren Informationen vorhanden&
        Überhitzung Leiterplattentemperatursensor7                             Keine weiteren Informationen vorhanden&
        Gradientenüberschreitung Leiterplattentemperatursensor"                    Keine weiteren Informationen vorhanden&
        Kabinentemperatursensor fehlerhaft                                         Keine weiteren Informationen vorhanden&
        Gradientenfehler Flammenwächter                                       Keine weiteren Informationen vorhanden&
        Emergency-Cooling                                                     Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 1                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 2                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 3                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 4                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 5                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 6                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 7                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 8                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 9                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 10                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 11                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 12                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 13                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 14                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 15                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 16                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 17                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 18;                                          Keine weiteren Informationen vorhanden&
        CAN-Kommunikation mit Steuergerät ECM/PCM "A" unterbrochenK           Keine weiteren Informationen vorhanden&
        undenspezifischer Fehler 20                                           Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 21                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 22                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 23                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 24                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 25                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 26                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 27                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 28                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 29                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 30                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 31                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 32                                          Keine weiteren Informationen vorhanden-
        Kundenspezifischer Fehler 33                                          Kundenfehlerspeicher mit Kundendiagnose auslesen7
        EOL-Checksummenfehler                                                 Der EOL-Datensatz im EEPROM hat einen Prüfsummenfehler
        kein Start im Testlauf                                                Kein Start im TestlaufI
        mehrfacher Flammabbruch                                               Die Flamme ist währ eines Heizzyklus mehr als FAZ (EEPROM) mal erloschenC
        Unterspannungsabschaltung                                             Die Betriebsspannung war zu lange unter dem zulässigen MinimalwertK
        Flamme nach Brennbetrieb.                                              Flamme nach Brennbetrieb (z.B. mech. blockiertes Kühlmittelumschaltventil)΁
        überhöhte Wassertemperatur ohne Brennbetrieb2                          Fehler wird gesetzt, wenn in Regelpause eine Kühlmitteltemperatur von 145°C überschritten wird. Erfolgt dies 3x hintereinander über einen Zeitraum von 100ms geht das Gerät in Überhitzungsverriegelung.6
        permanente Heizgeräteverriegelung wurde aktiviert)                     Die permanente Heizgeräteverriegelung wurde aktiviertP
        DP Unterbrechung oder Kurzschluss nach UB*                             Der Dosierpumpenstromkreis ist unterbrochen\noder hat einen Kurzschluss nach +UbU
        BLG Unterbrechung oder Kurzschluss nach UB2                                Der Brennluftgebläsestromkreis ist unterbrochen\noder hat einen Kurzschluss nach +Ub_
        GS/FW Unterbrechung oder einen Kurzschluss nach UB)                        Der Glühstift- / Flammwächterstromkreis ist unterbrochen\noder hat einen Kurzschluss nach +UbQ
        UP Unterbrechung oder Kurzschluss nach UB*                             Der Umwälzpumpenstromkreis ist unterbrochen\noder hat einen Kurzschluss nach +UbS
        EKP Unterbrechung oder Kurzschluss nach UB1                                Die elektronische Kraftstoffpunpe (EKP) hat Unterbrechung oder Kurzschluss nach +UbT
        grüne LED Unterbrechung oder Kurzschluss nach UB0                      Der Stromkreis der grünen LED ist unterbrochen\noder hat einen Kurzschluss nach +UbS
        gelbe LED Unterbrechung oder Kurzschluss nach UB                           Der Stromkreis der gelben LED ist unterbrochen\noder hat einen Kurzschluss nach +Ub&
        Fehler 143U                                                                Keine weiteren Informationen vorhanden]
        Kühlmittelumschaltventil Unterbrechung oder Kurzschluss nach UB, event. Überhitzung-                           Der Kühlmittelumschaltventilstromkreis ist unterbrochen\noder hat einen Kurzschluss nach +UbU
        neutralcodiertes oder gesperrtes Steuergerät                               Neutralcodiertes oder gesperrtes Steuergerät\nBei Auftreten des Fehlers kein Betriebg
0x92    Kommando aufrechterhalten fehlgeschlagenZ                              Kommando aufrechterhalten fehlgeschlagen.\nBei Auftreten des Fehlers kein Betrieb bzw. Störabschaltung^
        Verhältnis von Wärmetauschertemperatur zur Ansaugtemperatur im Heizbetrieb nicht korrekt8                  Verhältnis von Wärmetauschertemperatur zur Ansaugtemperatur im Heizbetrieb ist nicht korrektJ
        Temperatur-Sensor Unterbrechung oder Kurzschluss nach UB&              Der Temperatursensor ist unterbrochen\noder hat einen Kurzschluss nach +Ub7
        BM Kommunikationsfehler/Drehzahlfehler                                 Brennluftgebläse Kommunikationsfehler / Drehzahlfehler&
        Fehler 150Ü                                                           Keine weiteren Informationen vorhandenB
        HS-Gradient zu klein7                                                  Falsche Position des Überhitzungssensors (ÜHS-Gradient zu klein)3
        Kd.-spezif. Fehler, VW: CAN-Sollwertgeber-Unterbrechung:                   Unterbrechung oder Auslösung ÜberhitzungsschalterV
        Glüh-/Zündelement Unterbrechung oder Kurzschluss nach UB6              Der Glühstift- / Zündfunkgeber ist unterbrochen\noder hat einen Kurzschluss nach +UbS
        Brennraumsensor Unterbrechung oder Kurzschluss nach UB3                    Der Brennraumsensorstromkreis ist unterbrochen\noder hat einen Kurzschluss nach +UbY
        Sollwertpoti Unterbrechung oder Kurzschluss nach UBM                       Der Sollwertpotentiometerstromkreis ist unterbrochen\noder hat einen Kurzschluss nach +UbU
        verfügbare Heizzeit der intelligenten Unterspannungsabschaltung aufgebrauchtA  Die verfügbare Heizzeit der intelligenten Unterspannungsabschaltung ist aufgebrauchth
        Kraftstoffabsperrventil Shed-Test Unterbrechnung oder Kurzschluss7     Das Kühlmittelumschaltventil Shed-Test Stromkreis ist unterbrochen\noder hat einen Kurzschluss nach +UbT
        Brennstoffsensor Unterbrechung oder Kurzschluss nach UB9                   Der Brennstoffsensorstromkreis ist unterbrochen\noder hat einen Kurzschluss nach +UbP
        Düsenstockheizung Unterbrechung oder Kurzschluss nach UB6              Die Düsenstockvorwärmung ist unterbrochen\noder hat einen Kurzschluss nach +UbI
        Betriebsanzeige Unterbrechung oder Kurzschluss nach UB3                    Die Betriebsanzeige ist unterbrochen\noder hat einen Kurzschluss nach +UbF
        Flammanzeige Unterbrechung oder Kurzschluss nach UB3                       Die Flammanzeige ist unterbrochen\noder hat einen Kurzschluss nach +Ub&
        CAN-Kommunikation mit Steuergerät IPC unterbrochen                    Keine weiteren Informationen vorhanden&
        RESET durch Watchdog                                                  Keine weiteren Informationen vorhanden&
        RESET durch W-Bus m42s0A5                                              Keine weiteren Informationen vorhandenO
        BS-Vorwärmung Unterbrechung oder Kurzschluss nach UBE                  Die Brennstoffvorwärmung ist unterbrochen\noder hat einen Kurzschluss nach +UbW
        Leiterplattentemperatur-Sensor Unterbrechung oder Kurzschluss nach UB$ Der Leiterplattentemperatursensor ist unterbrochen\noder hat einen Kurzschluss nach +Ub&
        Ausstieg zum Loadermodus durch W-Bus/                                  Keine weiteren Informationen vorhanden/
        keine Kommunikation zum Bordnetz Energiemanager$                           Keine Kommunikation zum Bordnetz Energiemanagerǁ
        unzureichender Kühlmitteldurchfluss                                   Der Fehler tritt auf, wenn die Kühlmitteltemperatur während den Phasen Start, GPR (Glühstiftrampe) oder FMM (Flammwächtermessphase) die Schaltschwelle zum Ausbrennen in Regelpause überschreitet.m
        Senden auf W-Bus fehlgeschlagen:                                           Senden auf W-Bus fehlgeschlagen (auch nach viermaliger Wiederholung des Telegramms keine bzw. gestörte Antwort)W
        Überhitzungssensor Unterbrechung oder Kurzschluss nach UB2             Der Überhitzungssensorstromkreis ist unterbrochen\noder hat einen Kurzschluss nach +Ub;
        Drucksensor Unterbrechung oder Kurzschluss nach UB                     Der Drucksensor hat Unterbrechung oder Kurzschluss nach +Ub&
        Fehler 173                                                             Keine weiteren Informationen vorhanden&
        Fehler 174                                                             Keine weiteren Informationen vorhanden&
        Fehler 175                                                             Keine weiteren Informationen vorhanden&
        Fehler 176                                                             Keine weiteren Informationen vorhanden&
        Fehler 1773                                                                Keine weiteren Informationen vorhanden&
        CAN-Kommunikation mit Steuergerät BCM unterbrochen                     Keine weiteren Informationen vorhanden&
        Fehler 179                                                             Keine weiteren Informationen vorhanden&
        Fehler 180                                                             Keine weiteren Informationen vorhanden&
        Fehler 181:                                                                Keine weiteren Informationen vorhanden!
        Temperatursicherung Unterbrechung&                                     Temperatursicherung Unterbrechung oder Kurzschluss nach UB
        Fehler 183                                                             Keine weiteren Informationen vorhanden&
        Fehler 184                                                             Keine weiteren Informationen vorhanden&
        Fehler 185                                                             Keine weiteren Informationen vorhanden&
        Fehler 186                                                             Keine weiteren Informationen vorhanden&
        Fehler 187                                                             Keine weiteren Informationen vorhanden&
        Fehler 188                                                             Keine weiteren Informationen vorhanden&
        Fehler 189                                                             Keine weiteren Informationen vorhanden&
        Fehler 190                                                             Keine weiteren Informationen vorhanden&
        Fehler 191                                                             Keine weiteren Informationen vorhanden&
        Fehler 192                                                             Keine weiteren Informationen vorhanden&
        Fehler 193                                                             Keine weiteren Informationen vorhanden&
        Fehler 194                                                             Keine weiteren Informationen vorhanden&
        Fehler 195                                                             Keine weiteren Informationen vorhanden&
        Fehler 196                                                             Keine weiteren Informationen vorhanden&
        Fehler 197                                                             Keine weiteren Informationen vorhanden&
        Fehler 198                                                             Keine weiteren Informationen vorhanden&
        Fehler 199                                                             Keine weiteren Informationen vorhanden&
        Fehler 200                                                             Keine weiteren Informationen vorhanden&
        Fehler 201                                                             Keine weiteren Informationen vorhanden&
        Fehler 202                                                             Keine weiteren Informationen vorhanden&
        Fehler 203                                                             Keine weiteren Informationen vorhanden&
        Fehler 204                                                             Keine weiteren Informationen vorhanden&
        Fehler 205                                                             Keine weiteren Informationen vorhanden&
        Fehler 206                                                             Keine weiteren Informationen vorhanden&
        Fehler 207                                                             Keine weiteren Informationen vorhanden&
        Fehler 208                                                             Keine weiteren Informationen vorhanden&
        Fehler 209+                                                                Keine weiteren Informationen vorhandent
        falsche CAN-Network Master Configuration-ID                                Die über CAN gesete Fahrzeugkonfiguration stimmt mit der in der Heizung gespeicherten Konfiguration nicht überein.&
        Fehler 211                                                            Keine weiteren Informationen vorhandenށ
        Differenzspannung zu groß                                              Die Differenz zwischen gemessener und über Kundenbus empfangener Betriebsspannung ist zu groß.\nDie maximal erlaubte Differenz muß in dem jeweiligen Applikationslastenheft definiert sein.&
        Fehler 213                                                             Keine weiteren Informationen vorhanden&
        Fehler 214                                                             Keine weiteren Informationen vorhanden&
        Fehler 215                                                             Keine weiteren Informationen vorhanden&
        Fehler 216                                                             Keine weiteren Informationen vorhanden&
        Fehler 217                                                             Keine weiteren Informationen vorhanden&
        Fehler 218                                                             Keine weiteren Informationen vorhanden&
        Fehler 219                                                             Keine weiteren Informationen vorhanden&
        Fehler 220                                                             Keine weiteren Informationen vorhanden&
        Fehler 221                                                             Keine weiteren Informationen vorhanden&
        Fehler 222                                                             Keine weiteren Informationen vorhanden&
        Fehler 223                                                                Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 33                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 34                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 35                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 36                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 37                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 38                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 39                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 40                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 41                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 42                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 43                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 44                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 45                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 46                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 47                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 48                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 50                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 51                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 52                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 53                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 54                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 55                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 56                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 57                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 58                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 59                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 60                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 61                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 62                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 63                                          Keine weiteren Informationen vorhanden&
        Kundenspezifischer Fehler 64                                          Keine weiteren Informationen vorhanden4
        Unbekannter Fehlercode                                                Das Steuergerät meldet einen unbekannten Fehlercode
        <free>
 * */

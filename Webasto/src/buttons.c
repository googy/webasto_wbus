/*
 * buttons.c
//-------------buttons-debounce------------
 *
 *  Created on: 24.01.2020
 *      Author: googy
 */

#include <avr/io.h>
#include "buttons.h"


//FIXME anders regeln
uint8_t b1 = 0;
enum buttonstate b1_s = RELEASED;
uint8_t b2 = 0;
enum buttonstate b2_s = RELEASED;
uint8_t b3 = 0;
enum buttonstate b3_s = RELEASED;
uint8_t b4 = 0;
enum buttonstate b4_s = RELEASED;

void init_buttons() {
    // initialisiere buttons
    DDRD &= ~(1 << PD6);    // button1
    PORTD |= (1 << PD6);    // pullup
    DDRD &= ~(1 << PD5);    // button2
    PORTD |= (1 << PD5);
    DDRD &= ~(1 << PD4);    // button3
    PORTD |= (1 << PD4);
    DDRD &= ~(1 << PD3);    // button4
    PORTD |= (1 << PD3);
}

void button(enum buttonstate* b_state, uint8_t* b_count, uint8_t b_pin) {
    uint8_t pins = PIND;
    switch (*b_state) {
    case RELEASED:
        if (!(pins & (1 << b_pin))) {
            (*b_count)++;
            if (*b_count > 7) *b_state = RISING;
        } else {
            if (*b_count) (*b_count)--;
        }
        break;
    case RISING:
        break;
    case PRESSED:
        if (!(pins & (1 << b_pin))) {
            if (*b_count < 7) (*b_count)++;
        } else {
            if (*b_count) (*b_count)--;
            else *b_state = RELEASED;
        }
        break;
    case FALLING:
        break;
    }
}

void buttons_update() {
    button(&b1_s, &b1, PB6);
    button(&b2_s, &b2, PB5);
    button(&b3_s, &b3, PB4);
    button(&b4_s, &b4, PB3);
}

uint8_t b_event(uint8_t b_num) {
    switch (b_num) {
    case 1:
        if (b1_s == RISING) {
            b1_s = PRESSED;
            return 1;
        }
        break;
    case 2:
        if (b2_s == RISING) {
            b2_s = PRESSED;
            return 1;
        }
        break;
    case 3:
        if (b3_s == RISING) {
            b3_s = PRESSED;
            return 1;
        }
        break;
    case 4:
        if (b4_s == RISING) {
            b4_s = PRESSED;
            return 1;
        }
        break;
    }
    return 0;
}

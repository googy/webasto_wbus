/*
 * timebase.h
 *
 *  Created on: 24.01.2020
 *      Author: googy
 */

#ifndef SRC_TIMEBASE_H_
#define SRC_TIMEBASE_H_


void init_tick_timer();
void timer_init(uint32_t* counter);
uint8_t timer_expired(uint32_t* counter, uint16_t ms_time);

#endif /* SRC_TIMEBASE_H_ */

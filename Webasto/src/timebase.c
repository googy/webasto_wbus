/*
 * timebase.c
 *
 *  Created on: 24.01.2020
 *      Author: googy
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include "buttons.h"

//-----------------------------timebase----------------------------------------------------------------
volatile uint8_t time1 = 0;
volatile uint32_t ticks = 0;
// 16000000Hz / 1024 / 256 = 61Hz 61.03515625
ISR(TIMER2_OVF_vect) {
    time1++;             // tick 16,384ms, wrap around 4,194304s
    ticks++;
//    button(&b1_s, &b1, PB6);
//    button(&b2_s, &b2, PB5);
//    button(&b3_s, &b3, PB4);
//    button(&b4_s, &b4, PB3);
    buttons_update();
}

void init_tick_timer() {
    TCCR2A = 0;     // normal mode, no output compare output
    TCCR2B = (1 << CS22) | (1 << CS21) | (1 << CS20);   // 1024 prescaler
    TIMSK2 = 1 << TOIE2;                                // enable Timer2 Overflow Interrupt
}

// speichere aktuelle Zeit in Ticks im Counter
void timer_init(uint32_t* counter) {
    cli();
    *counter = ticks;
    sei();
}

// wenn die aktuelle Zeit den gespeicherten Startwert + offset übersteigt return true
uint8_t timer_expired(uint32_t* counter, uint16_t ms_time) {
    uint8_t ergebnis = 0;
    cli();
    if (ticks > (*counter + (ms_time >> 4))) ergebnis = 1;
    sei();
    return ergebnis;
}
//-----------------------------------------------------------------------------------------------------

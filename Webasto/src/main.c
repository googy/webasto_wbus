/*
 * geschrieben f�r ein Arduino Pro mini 5V, 16MHz
 * keine Arduino IDE, AVR-GCC Compiler und usbasp als Programmer
 * zus�tzlich optional als Interface ein 0.91 Zoll SSD1306 128x32 OLED Display mit I2C angebunden
 * Interface zum Zuheizer siehe beigelegte Schaltug, wobei ich w�rde f�r R19 einen etwas h�heren Wert nehmen
 */
#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <inttypes.h>

#include "uart.h"
#include "ssd1306.h"
#include "timebase.h"
#include "buttons.h"

#include "wbus.h"
char buf[10];
uint8_t message[150];

uint8_t temperatur = 0;
extern struct heater_state hs;


uint8_t msg[129];
uint8_t passive_rcv(void);

enum states {
    INIT = 0,
    CONNECT = 1,
    CONNECTED = 2,
    STANDHEIZEN_START = 3,
    STANDHEIZEN = 4,
    STOP = 5,
    ZUHEIZEN_START = 11,
    ZUHEIZEN = 12,
    DEBUG = 22,
};

int main() {

    // initialisiere Zeitgeber f�r Fehlererkennung
    init_tick_timer();  // Timer0 overflow Interrupt dient als Zeitgeber
    sei();              // unmask Interrupts to Processor, damit Interrupts auch ankommen

    init_buttons();     // Taten zur Steuerung

    setup();            // initialisierung OLED SPI Display
    oled_clear_display();   // l�sche Displayinhalt zur Initialisierung
    oled_set_start_addr(0 % 8, 0 % 128);    // 8 Zeilen
    oled_set_start_addr(4 % 8, 0 % 128);
    oledWriteString("1. ");
    oled_set_start_addr(5 % 8, 0 % 128);
    oledWriteString("2. sniff");
    oled_set_start_addr(6 % 8, 0 % 128);
    oledWriteString("3. ");
    oled_set_start_addr(7 % 8, 0 % 128);
    oledWriteString("4. connect");

    uint8_t tries = 0;      // Z�hler f�r Verbindungsversuche, wenn mehrfach keine Verbindung m�glich, wird abgebrochen
    enum states state = INIT;      // Zustandsvariable des Haupt-Zustandsautomaten, der die Abfolge der Aktionen regelt
    uint32_t counter1 = 0;
    uint32_t counter2 = 0;

    while (1) {
        switch (state) {    // Haupt Zustandsautomat
        case INIT: // off state, Startzustand, idle Zustand
            if (b_event(4)) {   // dr�cke Taste 4 um eine Verbindung aufzubauen
                state = CONNECT;      // gehe zum n�chsten Zustand
                tries = 0;      // reset Anzahl Verbindungsversuche, damit wie diese neu z�hlen, diese k�nnten von vorherigen Versuchen nicht auf 0 sein
                // gebe Information auf dem Display aus
                oled_clear_display();
                oled_set_start_addr(0 % 8, 0 % 128);
                oledWriteString("connecting");
                timer_init(&counter1);      // starte Timer, 2s zwischen Verbindungsversuchen.
            }

            if (b_event(2)) {   // dr�cke Taste 2 Sniffing mode, dient dazu die Kommunikation der ThermoTest Software auszuwerten, kann im Produktivsystem entfernt werden
                DDRC |= (1 << PC5); // TX f�r SoftwareUART, diese wird genutzt um Nachrichten an den PC zu senden. HardwareUART wird f�r W-Bus verwendet
                state = DEBUG;
                // passiver Modus, nur RX
                UBRR0H = UBRR_VAL >> 8;
                UBRR0L = UBRR_VAL & 0xFF;
                UCSR0B |= (0 << TXEN0) | (1 << RXEN0); // RX-interrupt, RX einschalten
                UCSR0C = (1 << UCSZ01) | (1 << UCSZ00) | (1 << UPM01); // Asynchron 8N1
            }
            break;

        case CONNECT: //connecting, baue Verbindung zum Zuheizer auf
            if (!timer_expired(&counter1, 2000)) {  // f�ge 2s Abstand zwischen Verbindungsversuchen
                // Wenn Timer nicht abgelaufen, breche die Ausf�hrung ab
                break;
            }
            // diese Stelle wird erreicht, wenn 2s abgelaufen sind

            // init, Verbindung zum WBus aufbauen
            if (init_webasto() != SUCCESS) {    // versuche zu verbinden, init_webasto() kehrt mit SUCCESS zur�ck, falls Verbindung erfolgreich, die erste Verbindung ist bei mir immer nicht erfolgreich, die zweite funktioniert aber

                // falls Verbindung nicht erfolgreich, versuche nochmal
                tries++;    // z�hle Verbindungsversuche
                timer_init(&counter1);  // starte timer um 2s Abstand zwischen Verbindungsversuchen einzubauen.
                oled_set_start_addr(3, 0);
                oledWriteString(itoa(tries, buf, 10));
                oledWriteString(". Versuch vehlgeschlagen");

                // falls 4 Versuche fehlgeschlagen, go idle
                if (tries >= 4) {
                    state = INIT;  // idle Zustand
                    oled_clear_display();
                    oled_set_start_addr(0, 0);
                    oledWriteString("Verbindungsfehler");
                }
            } else {
                // Verbindung erfolgreich
                oled_clear_display();
                oled_set_start_addr(0 % 8, 0 % 128);
                oledWriteString("connected");
                state = CONNECTED;  // nun sind wir verbunden und k�nnen kommunizieren
                timer_init(&counter1);  // 1,2s Abstand zur n�chsten Abfrage
                oled_set_start_addr(4 % 8, 0 % 128);
                oledWriteString("1. STOP");
                oled_set_start_addr(5 % 8, 0 % 128);
                oledWriteString("2. 31");
                oled_set_start_addr(6 % 8, 0 % 128);
                oledWriteString("3. Zuheizen");
                oled_set_start_addr(7 % 8, 0 % 128);
                oledWriteString("4. Standheizen");

            }
            break;

        case CONNECTED: // idle connected, Verbindung aufgebaut aber keine Aktion gestartet, hier w�re es angebracht alle Fehler auszulesen
            if (timer_expired(&counter1, 1200)) {    // ca. alle 1200ms Informationen von der Heizung abfragen
                if (request_data() == SUCCESS) {
                    oled_clear_line(0);
                    oled_clear_line(1);
                    oled_clear_line(2);
                    oled_set_start_addr(0, 0);
                    // Spannung
                    oledWriteString("Spannung: ");
                    oledWriteString(itoa(hs.voltage, buf, 10));

                    oled_set_start_addr(1, 0);
                    oledWriteString("State: ");
                    oledWriteString(itoa(hs.running_state, buf, 16));
                } else {
                    oled_clear_display();
                    oled_set_start_addr(0, 0);
                    oledWriteString("Verbindungsfehler");
                    state = INIT;
                }
                timer_init(&counter1);
            } // Datenabfrage Ende

            if (b_event(1)) {   // Taste 1 gedr�ckt, trenne Verbindung indem man nichts mehr sendet. Es bedarf keiner Abmeldung. Sobald der interne Timeout abgelaufen ist, muss man sich neu verbinden. Noch nicht getestet wie lang dieser ist, ich denke irgendwas um 8-10s
                state = INIT;
                oled_clear_display();
                oled_set_start_addr(0, 0);
                oledWriteString("GO INIT");
            }

            // falls Taste 4 gedr�ckt. Gehe zum n�chsten Zustand. Schalte Heizung ein.
            if (b_event(4)) {
                if (hs.running_state == 0x71) {
                    state = STANDHEIZEN_START; // Heizung bereit, schalte ein
                } else {
                    oled_clear_display();
                    oled_set_start_addr(0, 0);
                    oledWriteString("Heizung nicht bereit");
                    // Heizung nicht bereit, bleibe im aktuellen Zustand
                    _delay_ms(2000);
                }
            }

            // alternativ zum Test Zuheizer statt Standheizung einschalten
            if (b_event(3)) {
                oled_clear_line(0);
                oled_clear_line(1);
                oled_clear_line(2);
                oled_clear_line(3);
                oled_set_start_addr(0, 0);
                if (hs.running_state != 0x71) {
                    oledWriteString("Starten nicht Moglich da Heizung nicht bereit, warte ...");
                    _delay_ms(1000);
                } else {
                    state = ZUHEIZEN_START;
                }
            }

            if (b_event(2)) {
                state = 31;
            }
            break;

        case STANDHEIZEN_START: // Standheizung einschalten
            oled_clear_display();
            oled_set_start_addr(0, 0);
            oledWriteString("einschalten");

            _delay_ms(100);
            // Sende Einschaltkommando
            uart_puts("\xF4\x03\x21\x3B\xED");  // standheizen
            while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
            _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
            uart_flush();       // flush RX buffer
            if (rcv() == SUCCESS) {
                /////////////////////////////////////////////
                oled_clear_display(2, 0);
                for (uint8_t i = 0; i <= (message[1]) + 1; i++) {
                    oledWriteString(itoa(message[i], buf, 16));
                    oledWriteString(" ");
                }
                _delay_ms(3000);
                /////////////////////////////////////////////
                if (message[2] == 0xA1) {//answer 4F 03 A1 3B D6
                    state = STANDHEIZEN;
                    // Starten erfolgreich
                    timer_init(&counter1);  // wird im Zielzustand verwendet, stoppt Zeit zwischen Nachrichten
                    timer_init(&counter2);  // für keepalive
                } else {
                    // Starten nicht erfolgreich
                    oled_clear_display();
                    oled_set_start_addr(0, 0);
                    oledWriteString("Heizung nicht bereit");
                    _delay_ms(2000);
                    state = CONNECTED;
                }
            } else {
                oled_clear_display();
                oled_set_start_addr(0, 0);
                oledWriteString("Kommunikationsfehler");
                _delay_ms(2000);
                //state = INIT;
            }
            break;

        case STANDHEIZEN: // keep alive, w�hrend dem Heizen
            if (timer_expired(&counter1, 1200)) {    // ca. alle 1200ms Informationen von der Heizung abfragen
                // Datenabfrage, optional

                if (request_data() == SUCCESS) {
                    oled_clear_line(0);
                    oled_clear_line(1);
                    oled_clear_line(2);
                    oled_clear_line(3);

                    oled_set_start_addr(0, 0);
                    oledWriteString("Spannung: ");
                    oledWriteString(itoa(hs.voltage, buf, 10));

                    oled_set_start_addr(1, 0);
                    oledWriteString("State: ");
                    oledWriteString(itoa(hs.running_state, buf, 16));

                    oled_set_start_addr(2, 0);
                    oledWriteString("Power: ");
                    oledWriteString(itoa(hs.power, buf, 10));

                    oled_set_start_addr(3, 0);
                    oledWriteString("T: ");
                    oledWriteString(itoa(hs.temperature, buf, 10));


                } else {
                    oled_clear_display();
                    oled_set_start_addr(0, 0);
                    oledWriteString("Verbindungsfehler");
                    state = INIT;
                }

/*                  oledWriteString("Brennluft: ");
                    oledWriteString("Flamme: ");
                    oledWriteString("Spannung: ");
                    oledWriteString("Temperatur: ");
                    oledWriteString("Betriebszustand: ");

                    // TODO pr�fe ob Heizung wieder aus, falls ja melde dies und gehe in init
                    // bspw. wegen Fehler oder Zeit abgelaufen

                    // TODO if 5 mal Heizung aus, dann go idle
                    if (message[7] == 3) {
                        oledWriteString(" Zuh");
                    }
                    if (message[5] == 3) {
                        oledWriteString(" Sta");
                    } else {
                        oledWriteString(" non");
                    }
*/
                timer_init(&counter1);
                // Datenabfrage Ende
            }

            if (timer_expired(&counter2, 9000)) {   // keep alive Heizung, alle 10 sec. unbedingt n�tig, sonst geht Heizung wieder aus
                _delay_ms(300);
                uart_putb("\xF4\x04\x44\x21\x00\x95", 6);
                while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
                _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
                uart_flush();       // flush RX buffer
                rcv();
                // pr�fe Antwort ob alles in Ordnung
                // 4F 03 C4 01 89
                // sonst sende abschalten
                // gehe in state 0
                timer_init(&counter2);
            }

            // stoppe
            if (b_event(1)) {
                _delay_ms(150);
                uart_putb("\xF4\x02\x10\xE6", 4);
                while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
                _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
                uart_flush();       // flush RX buffer
                if (rcv() == SUCCESS) {
                    // 4F 02 90 DD
                }
                //TODO Warte bis Wait state erreicht wurde
                state = CONNECTED;
            }
            break;

            break;
            // wait till WAIT STATE
///////////////////////////////////////////////////////////////////////////////////////////////////////


        case ZUHEIZEN_START:
            // 4F 04 7F 24 11 01
            /* starte Zuhezbetrieb
             * Heizung w�rde nach 7 Sekunden ausgehen, wenn ken keepalife kommt */
            // Zuheizen
            _delay_ms(1200);
            uart_putb("\xF4\x03\x23\x3B\xEF", 5);
            while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
            _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
            uart_flush();       // flush RX buffer
            if (rcv() == SUCCESS) {

                oled_clear_display();
                oled_set_start_addr(0, 0);

                // gebe empfangene Nachricht aus
                for (uint8_t i = 0; i <= (message[1] + 1); i++) {
                    oledWriteString(itoa(message[i], buf, 16));
                    oledWriteString(" ");
                }

                // 4F 03 A3 3B D4 erfolgreich
                if (message[2] & 1 << 7) {
                    oled_set_start_addr(2, 0);
                    oledWriteString("OK");
                } else {
                    oled_set_start_addr(2, 0);
                    oledWriteString("FAIL");
                }

                state = ZUHEIZEN;
                timer_init(&counter1);
                timer_init(&counter2);
            } else {
                state = INIT;
            }
            break;

        case ZUHEIZEN:
            // keepalive
            if (timer_expired(&counter1, 1000)) {
                oled_clear_display();
                oled_set_start_addr(0, 0);
                oledWriteString("Zuh. keepalive");
                _delay_ms(150);
                uart_putb("\xF4\x04\x44\x23\x00\x97", 6);
                while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
                _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
                uart_flush();       // flush RX buffer
                if (rcv() == SUCCESS) {
                    // 4F 03 C4 01 89
                }

                _delay_ms(200);

//                uart_puts("\xF4\x04\x50\x30\x0C\x9C");  // read temp only
//                while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
//                _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
//                uart_flush();       // flush RX buffer
//                if (rcv() == SUCCESS) {
//                    // 4F 05 D0 30 0C 45 E3
//                    oled_set_start_addr(1, 0);
//                    for (uint8_t i = 0; i <= (message[1] + 1); i++) {
//                        oledWriteString(itoa(message[i], buf, 16));
//                        oledWriteString(" ");
//                    }
//                    oled_set_start_addr(3, 0);
//                    oledWriteString(itoa(message[5] - 50, buf, 10));    // empfangene Wert: Temperatur + 50
////                    0x12           25        Heizleistung 0-200%
////                    0x11        22 23        Heizleistung in Watt
////                    0x0E        17 18        Spannung Millivolt
//                    // es wird bis 75 Grad geheizt, dann wird die Leistung reduziert.
//                } else {
//                    oled_set_start_addr(3, 0);
//                    oledWriteString("fail");
//                }


                uart_puts("\xF4\x06\x50\x30\x0C\x0E\x28\xB8");  // read temp, voltage, state
                while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
                _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
                uart_flush();       // flush RX buffer
                if (rcv() == SUCCESS) {
                    //OK immer dann wenn (message[2] & 1 << 7)
                    oled_set_start_addr(1, 0);
                    for (uint8_t i = 0; i <= (message[1] + 1); i++) {
                        oledWriteString(itoa(message[i], buf, 16));
                        oledWriteString(" ");
                    }
                    oled_set_start_addr(3, 0);
                    oledWriteString("Temp: ");
                    oledWriteString(itoa(message[5] - 50, buf, 10));    // empfangene Wert: Temperatur + 50
                    oled_set_start_addr(4, 0);
                    oledWriteString("V: ");
                    oledWriteString(itoa(message[7] << 8 | message[8], buf, 10));
                    oled_set_start_addr(5, 0);
                    oledWriteString("State: ");
                    oledWriteString(itoa(message[10], buf, 16));

//                    0x12           25        Heizleistung 0-200%
//                    0x11        22 23        Heizleistung in Watt
//                    0x0E        17 18        Spannung Millivolt
                    // es wird bis 75 Grad geheizt, dann wird die Leistung reduziert.
                } else {
                    oled_set_start_addr(3, 0);
                    oledWriteString("fail");
                }


                timer_init(&counter1);
            }

            // sende Enschaltsignal f�r Zuheizung, dies sorgt daf�r, dass die Umw�lzpumpe an bleibt.
            // So wird der Motor vorgew�rmt.
            // Und die L�ftung bleibt aus, was Strom spart und der Motor wird schneller warm.
            if (timer_expired(&counter2, 5000)) {
                _delay_ms(150);
                uart_putb("\xF4\x03\x20\x3B\xEC", 5);
                while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
                _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
                uart_flush();       // flush RX buffer
                if (rcv() == SUCCESS) {
                    // if (4F 03 A0 3B D7)
                        // ok
                    // else
                        // nicht m�glich, evtl. Fehler
                        // state = 2 (idle connected)
                }
                timer_init(&counter2);
            }

            // stoppe
            if (b_event(1)) {
                // send stop
                // F4 02 10 E6
                // 15
                // 4F 02 90 DD
                _delay_ms(150);
                uart_putb("\xF4\x02\x10\xE6", 4);
                while (!(UCSR0A & (1 << UDRE0))) {} // warte bis alles gesendet wurde
                _delay_ms(5);   // die Antwort erfolgt ca. 15 ms nach dem Senden
                uart_flush();       // flush RX buffer
                if (rcv() == SUCCESS) {
                    // 4F 02 90 DD
                }

                // Warte bis Wait state erreicht wurde
                state = CONNECTED;
            }
            break;

        case STOP:
            break;

        case DEBUG:
            if (b_event(1)) {
                state = INIT;
            }

            if (passive_rcv()) {
                if (
                  //   msg[2] != 0x50 && msg[2] != 0xD0       // Datenabfrage
                  //&& msg[2] != 0x44 && msg[2] != 0xC4       // Standheizen
                  //&& msg[2] != 0x56 && msg[2] != 0xD6       // Fehlerverwaltung

                       msg[2] == 0xD0 && msg[3] == 0x30
                  ) {
                    soft_uart_puts(itoa(msg[39], buf, 16));
                    soft_uart_puts("\n\r");

                    //for (uint8_t i = 0; i <= (msg[1] + 1); i++) {
                    //    uart_s(itoa(msg[i], buf, 16));
                    //    uart_stx(' ');
                    //}
                    //uart_s(" xor:");
                    //uart_s(itoa(checksum(msg, msg[1] + 1), buf, 16));
                    //uart_s("\n\r");
                }
            } else
                soft_uart_puts("FAIL\n\r");
            // print message

            break;
        default:
            oled_clear_display();
            oled_set_start_addr(0, 0);
            oledWriteString("unhandeled state");
            _delay_ms(500);
            break;
        }
    }
    return 0;
}

/*
 * F�r debugging Zwecke. Software UART Ausgabe. Das Hardware UART Interface wird f�r die Kommunikation mit dem W-Bus benutzt.
 */
uint8_t passive_rcv() {
    uint8_t state = 0;
    uint8_t i = 0;
    uint8_t length = 0;

    while (1) {
        switch (state) {
        case 0: // warte auf erstes Byte 4F
            if (UCSR0A & (1 << RXC0)) { // Byte steht bereit, Zuheizer antwortet
                uint8_t c = UDR0;   // lese das empfangene Byte aus
                if ((c == 0x4F) | (c == 0xF4)) {    // FIXME sollte das Programm in der Mitte der Nachricht anfangen und es ist 4F oder F4 drin, kann dies ein Paar NAchrichten kaputt machen, sodass diese nicht empfangen werden k�nnen. Daher sollte mind. Pr�ffsumme berechnet werden.
                    msg[i] = c;  // f�r die checksum, ansonsten kann man auch drauf verzichten
                    i++;
                    state = 1;  // alles gut, als n�chstes empfangen wir die L�nge der Nachricht
                    break;
                }
            }
            break;
        case 1: // empfange die L�nge der Nachricht
            if (UCSR0A & (1 << RXC0)) {
                length = UDR0;
                msg[i] = length;
                i++;
                state = 2;
            }
            break;
        case 2: // empfange Daten
            // TODO timeout einbauen. Falls wir in der Mitte angefangen haben k�nnen wir hier dies erkennen.
            // wenn nicht alle Zeichen empfangen worden sind und es gibt eine zu gro�e L�cke in der �bertragung,
            // so k�nnen wir davon ausgehen, dass etwas schiefgelaufen ist und den Empfang abbrechen um auf die n�chste Nachricht zu warten
            /* if (timeout) {
                return false;
            } */

            if (UCSR0A & (1 << RXC0)) { // Zeichen bereit, nicht blockierend
                msg[i] = UDR0;          // lese aus
                if (i == (length + 1)) {    // Empfang abgeschlossen
                    state = 3;
                    break;
                }
                i++;
                break;
            }
            break;
        case 3: // pr�fe Integrit�t
            if (checksum(msg, msg[1] + 1) == msg[length + 1]) {
                return 1;
            }
            return 0;
        }
    }
}

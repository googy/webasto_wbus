/*
 * buttons.h
 *
 *  Created on: 24.01.2020
 *      Author: googy
 */

#ifndef SRC_BUTTONS_H_
#define SRC_BUTTONS_H_

/*
 * buttons.c
 *
 *  Created on: 24.01.2020
 *      Author: googy
 */

enum buttonstate {
    RELEASED = 1,
    RISING = 2,
    PRESSED = 3,
    FALLING = 4
};

void init_buttons();
void buttons_update();
void button(enum buttonstate* b_state, uint8_t* b_count, uint8_t b_pin);
uint8_t b_event(uint8_t b_num);

#endif /* SRC_BUTTONS_H_ */
